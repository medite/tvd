## Features
* Recherche dans le contenu
* Analyse du contenu: readability, # de mots, temps de lecture
* Meilleure intégration de toastr (ou encore [humane-js](https://github.com/wavded/humane-js))
* Ajouter des icones
* CSS utilisateur
* Serveur http pour gérér les entrées au site autrement que par le frontpage
* Publier le contenu sur un site public
* Effacer du contenu (fichier attaché)
* Support [hotkeys](https://github.com/jeresig/jquery.hotkeys)
* Fichiers attachés (autres formats que les images)
* URL (page.js) pour chaque fichier attaché sans le reste du site
* URL (page.js) pour le contenu sans le reste du site (?)
* Backlinks - tags, catégories
* Graphe de la page courante (liens entrants et sortants) - springy
* Stockage CouchDB/PouchDB
* Renommer une page
* Historique des pages
* Multilingue
* Importation et exportation, création et mises à jour du contenu
* Séparer ui.js en plusieurs modules
* Taille des dépendances JavaScript (vendor prend 336K après gulp, 1100K avant)
* Réduire la taille de jQuery utilisé (244K en ce moment)
* Réduire la taille de lodash utilisé (228K en ce moment)
* Réduire la taille de pouchdb utilisé (376K en ce moment)
* Réduire la taille de epiceditor (84K en ce moment) ou le remplacer

## Bugs
* Updater le contenu sans changer de DB
* EpicEditor 0.2.3 (nouveau - mais bug au retour du fullscreen)
* Ne pas permettre les autres tabs d'une page si le doc n'existe pas
* Update du contenu par défaut sans resetter la db (sans changer la db de nom)
* Trouver le problème de scrolling (après quelques pages, on est pris en haut de la page)
* Transformer pre/colophon.html en markdown (peut-être?)
* Collapser le menu principal sur click du menu
* Choix de DB flaky sur mobile (allDbs se reset tout seul)
* Race condition avec APP.setupAllDBs()

## Wishlist
* Mode squirt - spritz
* Mode slideshow - reveal, impress
* Carte graphique du site - springy
* Sous-pages (probablement pas)
* Co-édition
* Fédéré - décentralisé (voir [PeerPouch](https://github.com/natevw/PeerPouch) et [webrtc-peer](https://github.com/quartzjer/webrtc-peer))
* Auteurs
* Droits d'accès et d'édition (bien que ça ne veuille pas dire grand chose dans une app locale)
* Transclusion embed - part of - doc into another
* Table des matières - h1, h2, h3
* Breadcrumbs - chemin parcouru sur le site
* Utiliser browserify (au lieu de bower?)

## DONE
* Configuration initiale automatique
* [Frontpage](/)
* Backlinks
* Markdown - avec preview (epic editor)
* [Page d'aide](/aide) (début)
* [Contenu par défaut](/index)
* [Changements récents](/recent-changes)
* Navigation principale (accueil, récents, aide, etc.)
* Intégration de [headroom.js](http://wicky.nillia.ms/headroom.js/)
* Fichiers attachés (images)
* Mode hors-ligne - html5 app
* Contenu par défaut - supplantable
* Page colophon (automatique)
* Réarranger la sous-navigation dans chaque doc (voir, éditer, exporter, etc.)
* Recherche par URL (les bases pour la recherche de contenu)
* Liste des DB
* Permettre à l'utilisateur de sélectioner la DB
* Effacer du contenu (page/doc)
