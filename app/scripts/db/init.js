;(function(w) {
    /* jshint devel:true */
    'use strict';

    var created = [],
    createDoc = function(app, filename, title, path, fn) {
        var form = { title: title, path: path||'' };

        if (filename) {
            app.$.get(filename)
            .done(function(content) {
                form.content = content;
                form.links = app.extractLinks(content);
                app.db.update(app.ddocid + '/wikipage', {form:form}).then(function(a) {
                    created.push(JSON.parse(a.body));
                    if (fn) { fn(app); }
    //                toastr.success('UPDATE WIKIPAGE ' + title + ': ' + a.body);
                }, function(b) {
                    console.log('OUPSY UPDATE WIKIPAGE', b);
                });

            }).fail(function(data, a, b, c, d, e, f) {
                console.log('DOCS fail', data, a, b, c, d, e, f);
            });
        } else {
            form.content = '## Pages préinstallées\n\n';
            form.content = form.content + 'Les pages suivantes ont été créées au premier chargement avec la base de données.\n\n';
            form.content = form.content + '* [' + title + '](/' + path + ')\n';
            app._.each(created, function(c) {
                form.content = form.content + '* [' + c.title + '](/' + (c.path||'') + ')\n';
            });
            form.content = form.content + '* [Colophon](/colophon)\n';

            form.links = app.extractLinks(form.content);
            app.db.update(app.ddocid + '/wikipage', {form:form}).then(function(a) {
                created.push(JSON.parse(a.body));
                if (fn) { fn(app); }
//                toastr.success('UPDATE WIKIPAGE ' + title + ': ' + a.body);
            }, function(b) {
                console.log('OUPSY UPDATE WIKIPAGE', b);
            });
        }
    },
    scriptify = function(app, ddocjs) {
        if (ddocjs.views) {
            app._.forOwn(ddocjs.views, function(view, viewname) {
                if (app._.isFunction(view.map)) {
                    ddocjs.views[viewname].map = view.map.toString();
                }

                if (app._.isFunction(view.reduce)) {
                    ddocjs.views[viewname].reduce = view.reduce.toString();
                }
            });
        }

        if (ddocjs.updates) {
            app._.forOwn(ddocjs.updates, function(update, updatename) {
                if (app._.isFunction(update)) {
                    ddocjs.updates[updatename] = update.toString();
                }
            });
        }

        //FIXME
        // Also take care of all other functions (validates, lists, shows...)
    },
    createIndex = function(app) {
        createDoc(app, false, 'INDEX', 'index');
    },
    createDocs = function(cb, app) {
//        toastr.warning('should be <i>creating</i> other docs here');
        createDoc(app, 'docs/accueil.md', 'Accueil');
        createDoc(app, 'docs/ROADMAP.md', 'Roadmap', 'roadmap');
        createDoc(app, 'docs/aide.md', 'Aide', 'aide');
        createDoc(app, 'docs/TODO.md', 'TODO', 'todo', createIndex);
        cb(app);
    };

    w.initDDoc = function(app, mycb, newVersion, ddocinfo) {
        var ddoc = { _id: '_design/' + app.ddocid, META: {version: newVersion}};

        if (ddocinfo && ddocinfo.rev) {
            ddoc._rev = ddocinfo.rev;
        }

        app.$.getScript('scripts/db/ddoc.js')
        .done(function() {
            /* globals ddocjs */
            scriptify(app, ddocjs);
            app._.merge(ddoc, ddocjs);
            delete app.window.ddocjs;
            app.db.put(ddoc)
            .then(function() {
                if (!ddocinfo || !ddocinfo.rev) {
                    createDocs(mycb, app);
                } else {
                    mycb();
                }
            },
            function(b2) {
                console.log('PUT (er)', b2);
            });
        })
        .fail(function(a, b, c, d, e) {
            console.log('FAIL', a, b, c, d, e);
        });
    };
}(window));
