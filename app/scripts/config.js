;(function(w) {
    'use strict';
    w.APP.config = {
        ddocversion: '0.3.2',
        dbname:'tvd-1.0.0',
        dbplugins: {
            type:'only',
            plugins:'Update'
        }
    };
}(window));
