# Page d'aide pour Tsé veut dire

## Syntaxe du contenu
[Markdown](http://daringfireball.net/projects/markdown/syntax#img) (version [marked](https://github.com/chjj/marked))

Faire un lien "texte" vers la page /url: ```[texte](/url)```

Affichier une image: ```![texte alternatif](image.jpg)```

Formattage: ```*italique*``` ```**gras**```

```## titre niveau 2```

```### titre niveau 3```

## Hotkeys pour Epic Editor
* ALT-F pour Plein écran
* ESC pour sortir du mode Plein écran
* ALT-P pour un aperçu

## Changements locaux seulement
Les documents ne sont pas sauvegardés sur un serveur, mais via l'API Indexed DB de votre fureteur.

## Fonctions à venir
Voir le [TODO](/todo) pour les fonctions à venir et celles déjà complétées.
