/* globals APP, page, EpicEditor, Modernizr, PouchDB */

APP.$(function() {
    /* jshint devel:true */
    'use strict';

    var cb = function() {
        var edit = function(a) {
            var editor, $page = APP.$('.page', APP.$app), $textarea;
            if (!a.state.doc._id) {
                a.state.doc._id = '';
                a.state.doc._rev = '';
            }
            $page.append(APP.editTmpl(a.state.doc));
            $textarea = APP.$('#epicarea', $page);
            editor = new EpicEditor({
                theme: {editor: '/themes/editor/epic-light.css', preview: '/themes/preview/preview-dark.css'},
                autogrow: {minHeight: 100, maxHeight: function() { return Math.max(100, window.innerHeight - 250);}},
                button: { bar: (Modernizr && Modernizr.touch)?'show':'auto'},
                basePath: '/bower_components/epiceditor/epiceditor',
                textarea: $textarea[0],
                focusOnLoad: true
            }).load(function() {
                // on déplace le utilbar en haut (il se trouve en bas par défaut)
                APP.$('#epiceditor > iframe', $page).load( function() {
                    APP.$(this).contents().find('head')
                      .append(APP.$('<style type="text/css">#epiceditor-utilbar{top:10px}</style>'));
                });
                $textarea.hide();
            });
        },
        exporter = function(a) {
            var $page = APP.$('.page', APP.$app);
            // FIXME
            // active ne devrait pas être dans doc
            // spath n'est probablement pas nécessaire (on a path)
            delete a.state.doc.active;
            delete a.state.doc.spath;
            $page.append('<pre>' + JSON.stringify(a.state.doc, null, '    ') + '</pre>');
        },
        attacher = function(a) {
            var $page = APP.$('.page', APP.$app), data = {
                attachments: false,
                id: a.state.doc._id,
                rev: a.state.doc._rev,
                path: a.state.doc.path ? ('/' + a.state.doc.path) : ''
            };

            if (a.state.doc && a.state.doc._attachments) { data.attachments = a.state.doc._attachments; }
            $page.append(APP.attachmentsTmpl(data));            
        },
        show = function(a) {
            /* globals marked */
            var $page = APP.$('.page', APP.$app);

            if (a.state.doc.content) {
                $page.append(marked(a.state.doc.content));
            } else {
                page.redirect(a.path + '/edit');
            }
        },
        backlinks = function(a) {
            // stale: 'update_after'
            APP.db.query(APP.ddocid + '/backlinks', { key: a.state.doc.path }).then(function(c) {
                APP.$('.page', APP.$app).append(APP.backlinksTmpl({rows: c.rows}));
            });
        },
        loadImpl = function(a, b, doc) {
            var action, e = a.path.split('/').pop();

            APP.$('#recent-changes-nav').removeClass('active');

            if ('edit' === e) { action = 'editer'; }
            else if ('export' === e) { action = 'exporter'; }
            else if ('backlinks' === e) { action = 'backlinks'; }
            else if ('attach' === e) { action = 'attacher'; }
            else { action = 'voir'; }
            doc.active = {};
            doc.active[action] = ' class=active';
            doc.spath = a.params.page ? ('/' + a.params.page) : '';

            if (!a.state.doc) {
                a.state.doc = doc;
                a.save();
            }
            APP.$('title').html(doc.title + ', tséveutdire');
            APP.$('.page', APP.$app).html(APP.docnavTmpl(doc));
            b();
        },
        recentChanges = function(a) {
            APP.$('title').html('Changements récents, tséveutdire');
            APP.$('#recent-changes-nav').addClass('active');
            if (a.state.recentChanges) {
                APP.$('.page', APP.$app).html(APP.recentChangesTmpl({rows:a.state.recentChanges}));
            } else {
                APP.db.query(APP.ddocid + '/recentChanges', {descending: true}).then(function(c) {
                    APP.$('.page', APP.$app).html(APP.recentChangesTmpl({rows:c.rows}));
                    a.state.recentChanges = c.rows;
                    a.save();
                });
            }
        },
        colophon = function() {
            APP.$('#recent-changes-nav').removeClass('active');
            APP.$('title').html('Changements récents, tséveutdire');
            APP.$('.page', APP.$app).html(APP.colophonTmpl());
        },
        file = function(a) {
            APP.$('h1.title', APP.$app).append(' <small>' + a.params.file + '</small>');
            APP.db.getAttachment(a.state.doc._id, a.params.file, function(err, res) {
                var blobURL = URL.createObjectURL(res), newImg = document.createElement('img');
                newImg.src = blobURL;
                APP.$('.page', APP.$app).append(newImg);
            });
        },
        dbsfn = function dbsfn() {
            PouchDB.allDbs().then(function (dbs) {
                APP.$('.page', APP.$app).html(APP.dbsTmpl({dbname:APP.dbname, dbs:APP._.filter(dbs, function(d) {
                    return !d.indexOf('tvd-') && (d !== APP.dbname); // starts with tvd-
                })}));

                APP.$('#exampleModal').off('show.bs.modal');
                APP.$('#exampleModal').off('click', 'button');

                APP.$('#exampleModal').on('click', 'button', function() {
                    var $button = APP.$(this), $modal = APP.$('#exampleModal'), dbname = $modal.data('dbname');
                    if ($button.hasClass('btn-primary')) {
                        APP.$.cookie('dbname', dbname, {expires: 365});
                        $modal.modal('hide');
                        APP.window.location.href = '/'; // reload!
                    } else if ($button.hasClass('btn-danger')) {
                        PouchDB.destroy(dbname).then(function() {
                            $modal.modal('hide');
                            dbsfn();
                        });
                    }
                });

                APP.$('#exampleModal').on('show.bs.modal', function(event) {
                    var $button = APP.$(event.relatedTarget), // Button that triggered the modal
                        recipient = $button.text(), // Extract info from data-* attributes
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        $modal = APP.$(this),
                        db = new PouchDB(recipient),
                        $del = APP.$('button.btn-danger', $modal);

                    $del.popover({content:'<img src="images/loading.gif" alt="En attente de chargement">', html: true});
                    $modal.data('dbname', recipient);
                    db.info().then(function(i) {
                        $modal.find('.modal-title').text('DB info de ' + recipient);
                        $modal.find('.modal-body').html(APP.dbinfoTmpl(i));

                        /* jshint camelcase: false */
                        if (i.doc_count) {
                            db.get('_design/' + APP.ddocid).then(function(g) {
                                if (g && g.META && g.META.version) {
                                    $modal.find('.modal-body').append('<p><span class="label label-success">Version du ddoc: ' + g.META.version + '</span></p>');
                                } else {
                                    $modal.find('.modal-body').append('<p><span class="label label-warning">Sans version du ddoc</span></p>');
                                }
                            }).catch(function() {
                                $modal.find('.modal-body').append('<p><span class="label label-warning">Pas de ddoc</span></p>');
                            });
                        }
                    });
                });                
            });
        },
        load = function(a, b) {
            var opts = {
                    /* jshint camelcase: false */
                    include_docs: true,
                    limit: 1
                };

            if (!a.params.page) { a.params.page = ''; }
            opts.key = a.params.page;
            if (a.state.doc) { loadImpl(a, b, a.state.doc); }
            else {
                APP.db.query(APP.ddocid + '/byPath', opts).then(function(c) {
                    if (c.rows.length) { loadImpl(a, b, c.rows[0].doc); }
                    else {
                        // on va éventuellement rediriger à la page d'édition
                        // l'espace dans le contenu va forcer epiceditor à s'initaliser correctement
                        loadImpl(a, b, {title: a.params.page, path: a.params.page, content: ' '});
                    }
                }, function(e) {
                    console.log('ERROR', e);
                });
            }
        }, $go = APP.$('.navbar-fixed-top .typeahead')
        .typeahead({
            source: function(query, process) {
                APP.db.query(APP.ddocid + '/recentChanges').then(function(c) {
                    var out = ['/dbs', '/recent-changes', '/colophon'];
                    APP._.each(c.rows, function(r) {
                        out.push('/' + r.value.path);
                    });
                    process(APP._.sortBy(out));
                });
            },
            afterSelect: function(go) {
                page(go);
                $go.val('');
            }
        });

        $go.parents('form').on('submit', function(ev) {
            // quand le choix utilisateur n'est pas dans source de typeahead
            ev.preventDefault();
            page('/' + $go.val());
            $go.val('');
        });


        page('/', load, show);
        page('/edit', load, edit);
        page('/export', load, exporter);
        page('/backlinks', load, backlinks); // FIXME: should bypass load, but still display docnav
        page('/attach', load, attacher); // FIXME: should bypass load, but still display docnav
        page('/files/:file', load, file); // FIXME: should bypass load, but still display docnav
        page('/recent-changes', recentChanges);
        page('/colophon', colophon);
        page('/dbs', dbsfn);
        page('/:page', load, show);
        page('/:page/edit', load, edit);
        page('/:page/export', load, exporter);
        page('/:page/backlinks', load, backlinks);
        page('/:page/attach', load, attacher);
        page('/:page/files/:file', load, file); // FIXME: should bypass load, but still display docnav
        // todo: page qui retourne uniquement le fichier attaché (c'est présenté dans le cadre du site)
        page();
    };
    
    APP.$('.page', APP.$app).on('change', 'form.attach input[type=file]', function(ev) {
        var path, r, id, rev, $display = APP.$('div.show', APP.$app), ser = APP.$(this).parents('form').serializeArray();

        if (ev.target.files[0].type.match(/image.+/)) {            
            for (r = 0; r < ser.length; ++r) {
                if ('id' === ser[r].name) { id = ser[r].value; }
                else if ('rev' === ser[r].name) { rev = ser[r].value; }
                else if ('path' === ser[r].name) { path = ser[r].value; }
            }
            APP.db.putAttachment(id, ev.target.files[0].name, rev, ev.target.files[0], ev.target.files[0].type).then(function() {
                page(path + '/files/' + ev.target.files[0].name);
            });
        } else { $display.html('Le format de fichier ' + ev.target.files[0].type + ' n\'est pas supporté.'); }
    }).on('click', 'form.edit button.btn-danger', function() {
        var docid, rev, r, ser = APP.$(APP.$(this).parents('form')).serializeArray();

        for (r = 0; r < ser.length; ++r) {
            if ('docid' === ser[r].name) { docid = ser[r].value; if (rev) { break; }}
            else if ('rev' === ser[r].name) { rev = ser[r].value; if (docid) { break; }}
        }

        APP.db.remove(docid, rev).then(function() {
            page('/recent-changes');
        });
    }).on('submit', 'form.edit', function(ev) {
        var title, r, ser, content, docid, spath, path, form = {links:[]};
        ev.preventDefault();
        ser = APP.$(this).serializeArray();
        for (r = 0; r < ser.length; ++r) {
            if ('content' === ser[r].name) { content = ser[r].value; }
            else if ('docid' === ser[r].name) { docid = ser[r].value; }
            else if ('spath' === ser[r].name) { spath = ser[r].value; }
            else if ('path' === ser[r].name) { path = ser[r].value; }
            else if ('title' === ser[r].name) { title = ser[r].value; }
        }

        form.links = APP.extractLinks(content);
        form.content = content;
        if (path) { form.path = path; }
        if (title) { form.title = title; }

        APP.db.update(APP.ddocid + '/wikipage/' + docid, {form:form}).then(function() {
//            toastr.success('UPDATE WIKIPAGE: ' + a.body);
            page(spath||'/');
        }, function(b) {
            console.log('OUPSY UPDATE WIKIPAGE', b);
        });
    });

    if ('function' === typeof headroom && Modernizr && !Modernizr.touch) {
        APP.$('.navbar-fixed-top').headroom();
    }
    
    APP.init(cb);
});
