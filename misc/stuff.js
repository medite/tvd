            var w = new PouchDB('pouch__all_dbs__');
            w.info().then(function(a, b, c) {
                console.log('w.info():', a, b, c);
            });

            //PouchDB.resetAllDbs().then(function() {

                var x = new PouchDB('pouch__all_dbs__');
                x.info().then(function(a, b, c) {
                    console.log('x.info():', a, b, c);
                });
                PouchDB.allDbs().then(function (dbs) {
                    var y = new PouchDB('pouch__all_dbs__');
                    y.info().then(function(a, b, c) {
                        console.log('y.info():', a, b, c);
                    });

                    APP.$('.page', APP.$app).html(APP.dbsTmpl({dbname:APP.dbname, dbs:dbs}));

    //                APP.$('.page', APP.$app).off('click', 'button.add2reg');

                    APP.$('.page', APP.$app).off('click', 'button.resetreg');

                    if (dbs.length > 1 || (dbs.length && APP.dbname !== dbs[0])) {
                        APP.$('.page', APP.$app).append('<p><button class="resetreg btn btn-danger">Réinitialiser le registre?</button></p>');
                        APP.$('.page', APP.$app).on('click', 'button.resetreg', function(ev) {
                            console.log('resetting (1)...');
                            PouchDB.resetAllDbs().then(function() {
                                console.log('resetting (2)...');
                                dbsfn();
                            });
                        });
                    }

                    if (APP.window.indexedDB && APP.window.indexedDB.webkitGetDatabaseNames) {
                        APP.window.indexedDB.webkitGetDatabaseNames().onsuccess = function(sender) {
                            var pouches = APP._.chain(sender.target.result)
                            //.filter(function(value) {
                            //    return !value.indexOf('_pouch_tvd-'); // starts with
                            //})
                            .reject(function(value) {
    //                            return ('_pouch__allDbs' === value) || ('_pouch_pouch__all_dbs__' === value) || (-1 !== value.indexOf('-mrview-'));
                                return -1 !== value.indexOf('-mrview-');
                            })
                            .map(function(value) {
                                return value.slice(7);
                            })
                            .difference(dbs)
                            .value();//.slice(0, 10);

                            console.log('orig:', sender.target.result);
                            if (1||pouches.length) {
                                APP.$('.page', APP.$app).append('<p>Nous avons trouvé ' + pouches.length + ' autres DB. <button class="add2reg btn btn-primary">Les ajouter au registre?</button></p>');
                                console.log('webkitGetDatabaseNames (2)', JSON.stringify(pouches, null, '  '));

    //                            APP.$('.page', APP.$app).on('click', 'button.add2reg', function(ev) {
    //                                PouchDB.resetAllDbs().then(function() {
    //                                    dbsfn();
    //                                });
    //                                APP._.each(pouches, function(p) {
    //                                    new PouchDB(p);
    //                                });
    //                                setTimeout(dbsfn, 50);


    //                                page('/dbs?' + APP._.now());
    //                                console.log('ADD2REG (ev)', ev);
    //                                console.log('ADD2REG (this)', this);
    //                                console.log('ADD2REG ($this)', APP.$(this));
    //                                APP.$('.page', APP.$app).append('<pre>' + JSON.stringify(pouches, null, '  ') + '</pre>');
    //                            });
                            }
                        };
                    } else {
                        console.log('difficile de connaitre les db créées...', a, b);
                        APP.$('.page', APP.$app).append('<p><i>Difficile de connaitre les db créées:</i> <code>window.indexedDB.webkitGetDatabaseNames()</code> <i>n\'est pas disponible.</i></p>');

    if(0) {
                        // let's try 'em all
                        // tvd-0.0.0 to tvd-0.0.99 and tvd-0.1.0 to tvd-0.1.3
                        APP._.each(APP._.range(100), function(min) {
                            var dbname1 = 'tvd-0.0.' + min, db1 = new PouchDB(dbname1);
                            db1.info().then(function(i1) {
                                //console.log('DB INFO', i1);
                                if (0 === i1.update_seq) {
                                    db1.destroy().then(function(g) {
                                        console.log('DESTROYING ' + dbname1 + ': ' + g);
                                    });
                                }
                            });
                        });

                        APP._.each(APP._.range(4), function(min) {
                            var dbname1 = 'tvd-0.1.' + min, db1 = new PouchDB(dbname1);
                            db1.info().then(function(i1) {
                                //console.log('DB INFO', i1);
                                if (0 === i1.update_seq) {
                                    db1.destroy().then(function(g) {
                                        console.log('DESTROYING ' + dbname1 + ': ' + g);
                                    });
                                }
                            });
                        });
    }                    
                    }
                }).catch(function (err) {
                    console.log('allDbs err', err);
                });
//            });
