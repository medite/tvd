/* jshint node:true */
'use strict';
// generated on 2014-12-01 using generator-gulp-webapp 0.2.0
var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    fs = require('fs'),
    bower = require('bower'),
    _ = require('lodash'),
    npm = require('npm');


gulp.task('sloc', function(){
  gulp.src(['app/scripts/**/*.js'])
    .pipe($.sloc());
});

gulp.task('colophon', function() {
    return gulp.src('templates/pre/colophon.html')
        .pipe($.data(function(f, cb, undefined) {
            bower.commands.list().on('end', function(info) {
                var bowers = [];
                _.forOwn(info.dependencies, function(value) {
                    bowers.push({
                        name: value.pkgMeta.name,
                        version: value.pkgMeta.version,
                        homepage: value.pkgMeta.homepage,
                        authors: value.pkgMeta.authors,
                        license: value.pkgMeta.license,
                        description: value.pkgMeta.description
                    });
                });

                npm.load({json: true, long: true, depth: 1}, function() {
                    npm.commands.ls(false, true, function(a, b) {
                        var npms = [];
                        _.forOwn(b.dependencies, function(value) {
                            npms.push({
                                name: value.name,
                                version: value.version,
                                homepage: value.homepage,
                                authors: value.author,
                                license: value.license,
                                description: value.description
                            });
                        });
                        cb(undefined, {bower:bowers, npm:npms});
                    });
                });                
            });
        }))
        .pipe($.template())
        .pipe(gulp.dest('templates'));
});

gulp.task('edithtml', ['html'], function() {
  return gulp.src(['dist/index.html'])
    .pipe($.edit(function(src, cb) {
        cb(null, src.replace('<html class=no-js lang=fr>', '<html class=no-js lang=fr manifest="/app.manifest">'));
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('manifest', ['jshint', 'edithtml', 'images', 'fonts', 'extras', 'templates'], function(){
  return gulp.src(['dist/**'])
    .pipe($.manifest({
      hash: true,
      preferOnline: false,
      network: ['http://*', 'https://*', '*'],
      filename: 'app.manifest',
      exclude: 'app.manifest'
     }))
    .pipe(gulp.dest('dist'));
});

gulp.task('styles', function () {
  return gulp.src('app/styles/main.scss')
    .pipe($.plumber())
    .pipe($.rubySass({
      style: 'expanded',
      precision: 10
    }))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe(gulp.dest('.tmp/styles'));
});

gulp.task('jshint', function () {
  return gulp.src(['app/scripts/**/*.js', '!app/scripts/generated/templates.js', '!app/scripts/vendor/**/*.js'])
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.jshint.reporter('fail'));
});

gulp.task('html', ['styles'], function () {
  var lazypipe = require('lazypipe'),
      cssChannel = lazypipe()
    .pipe($.csso)
    .pipe($.replace, 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap','fonts'),
    assets = $.useref.assets({searchPath: '{.tmp,app}'});

  return gulp.src('app/*.html')
    .pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', cssChannel()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')().concat('app/fonts/**/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras4', function () {
  return gulp.src([
    'bower_components/epiceditor/epiceditor/themes/**/*.css',
  ], {
    dot: true
  })
  .pipe(gulp.dest('dist/bower_components/epiceditor/epiceditor/themes'));
});

gulp.task('extras3', function () {
  return gulp.src([
    'app/docs/*',
  ], {
    dot: true
  })
  .pipe(gulp.dest('dist/docs'));
});

gulp.task('extras2', function () {
  return gulp.src([
    'app/scripts/db/*.js',
  ], {
    dot: true
  })
  .pipe($.uglify())
  .pipe(gulp.dest('dist/scripts/db'));
});

gulp.task('extras', ['extras2', 'extras3', 'extras4'], function () {
  return gulp.src([
    'app/*.*',
    '!app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('connect', ['styles'], function () {
  var port = 9000,
      serveStatic = require('serve-static'),
      serveIndex = require('serve-index'),
      app = require('connect')()
    .use(require('connect-livereload')({port: 35729}))
    .use(serveStatic('.tmp'))
    .use(serveStatic('app'))
    // paths to bower_components should be relative to the current file
    // e.g. in app/index.html you should use ../bower_components
    .use('/bower_components', serveStatic('bower_components'))
    .use(serveIndex('app'));

  require('http').createServer(app)
    .listen(port)
    .on('listening', function () {
      console.log('Started connect web server on http://localhost:' + port);
    });
});

gulp.task('serve', ['connect', 'watch'], function () {
//  require('opn')('http://localhost:9000');
});

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('app/styles/*.scss')
    .pipe(wiredep())
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({exclude: ['bootstrap-sass-official']}))
    .pipe(gulp.dest('app'));
});

gulp.task('templates', ['colophon'], function () {
    gulp.src('templates/*.html')
        .pipe($.templateCompile({
            namespace: 'APP',
            name: function(f) {
                return f.relative.split('.')[0] + 'Tmpl';
            }
        }))
        .pipe($.concat('templates.js'))
        .pipe(gulp.dest('app/scripts/generated'));
});

gulp.task('watch', ['connect', 'templates', 'wiredep'], function () {
  $.livereload.listen();

  // watch for changes
  gulp.watch([
    'app/*.html',
    '.tmp/styles/**/*.css',
    'app/scripts/**/*.js',
    'app/images/**/*'
  ]).on('change', $.livereload.changed);

  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('bower.json', ['wiredep']);
  gulp.watch(['bower.json', 'package.json', 'templates/pre/colophon.html'], ['colophon', 'templates']);
  gulp.watch('templates/*.html', ['templates']);
});

gulp.task('build', ['manifest'], function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
