;(function(w) {
'use strict';

var ddocjs = {
    views: {
        /* globals emit */
        byPath: {
            map: function(doc) {
                if ('undefined' !== typeof doc.path) {
                    emit(doc.path);
                }
            }
        },
        recentChanges: {
            map: function(doc) {
                if ('undefined' !== typeof doc.path && doc.updated && doc.updated.length) {
                    emit(doc.updated[doc.updated.length - 1].date, {title:doc.title, path:doc.path, created:doc.created});
                }
            }
        },
        backlinks: {
            map: function(doc) {
                var r;
                if (doc.links && doc.links.length) {
                    for (r = 0; r < doc.links.length; ++r) {
                        if (doc.links[r] !== doc.path) {
                            emit(doc.links[r], {title:doc.title, path: doc.path});
                        }
                    }
                }
            }
        }
    },
    updates: {
        wikipage : function(doc, req) {
            var now = Date.now(), id = false, user = req.userCtx, msg;

            // called with updates/wikipage or updates/wikipage/ID ?
            if (req.id) {
                // called with ID
                // create or update doc
                if (!doc) {
                    id = req.id;
                }
            } else {
                // called without ID
                // create a new doc
                id = req.uuid;
            }

            if (id) {
                doc = {
                    _id: id,
                    created: now,
                    updated: [ ]
                };
                msg = 'doc created; ';
            } else {
                msg = 'doc updated; ';
            }

            user.peer = req.peer;
            doc.updated.push({date:now, user: user});

            if ('undefined' !== typeof req.form.links) {
                doc.links = req.form.links;
                msg = msg + 'updating links; ';
            }

            if ('undefined' !== typeof req.form.title) {
                doc.title = req.form.title;
                msg = msg + 'updating title; ';
            }

            if ('undefined' !== typeof req.form.content) {
                doc.content = req.form.content;
                msg = msg + 'updating content; ';
            }

            // path is not defined yet
            if ('undefined' === typeof doc.path) {
                msg = msg + 'path was not defined; ';
                if ('undefined' === typeof req.form.path) {
                    if (doc.title) {
                        // no path submitted, let's try to use the title
                        doc.path = doc.title.toLowerCase().replace(/[^ a-z]/g, '').replace(/ +/g, '-');
                    } else {
                        // no path submitted, let's use doc._id
                        doc.path = doc._id;
                        msg = msg + 'using doc._id; ';
                    }
                } else {
                    doc.path = req.form.path;
                    msg = msg + 'using submitted path (1); ';
                }
            } else {
                // we've got a path, but is a new one submitted?
                if ('undefined' === typeof req.form.path) {
                    msg = msg + 'keep the path; ';
                } else {
                    doc.path = req.form.path;
                    msg = msg + 'using submitted path (2); ';
                }
            }

//            console.log('MSG:', msg, doc);

            return [doc, { headers: { 'Content-Type' : 'application/json' }, body: JSON.stringify(doc)}];
        },
        hello : function(doc, req) {
            if (!doc) {
                if (req.id) {
                    return [{
                        _id : req.id
                    }, 'New World'];
                }
                return [{_id: req.uuid, world: 'Empty'}, 'Empty World'];
            }
            doc.world = 'hello';
            doc.editedBy = req.userCtx;
            return [doc, 'hello doc'];
        }
    }
};
w.ddocjs = ddocjs;
}(window));
