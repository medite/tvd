# Page d'accueil

Bienvenue sur **Tsé veut dire**, à votre service.

Un autre paragraphe, avant d'aller au prochain sous-titre.

Créer une page en ajoutant son lien à une page puis en suivant ce lien.
Par exemple, le lien suivant va [nul part](/nul-part), tant que vous ne sauvegarder pas ce document.

## Voici le sous-titre

* Un truc
* Deux affaires

Voir les [changements récents](/recent-changes), l'[aide](/aide) et l'[index des pages préinstallées](/index).
