Ces plugins pour PouchDB nous viennent de [
http://python-pouchdb.marten-de-vries.nl/plugins.html](http://python-pouchdb.marten-de-vries.nl/plugins.html)

Merci à Marten de Vries.

Documentation: [http://pythonhosted.org/Python-PouchDB/js-plugins.html](http://pythonhosted.org/Python-PouchDB/js-plugins.html).
