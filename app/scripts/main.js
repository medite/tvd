/* jshint devel:true */
;(function(w) {
    'use strict';
    /* globals PouchDB, _, initDDoc, marked */

/*
    toastr.options.positionClass = 'toast-bottom-center';
    toastr.options.timeOut = 10000; // How long the toast will display without user interaction
    toastr.options.extendedTimeOut = 60000; // How long the toast will display after a user hovers over it
    toastr.options.progressBar = true; 
*/

    w.APP = {
        $: jQuery.noConflict(),
        _: _, // _.noConflict() difficile à cause des templates précompilés (option imports)
        window: w,
        ddoc: false,
        ddocid: 'tvd',
        checkDDoc: function(version) {
            var self = this;
            return this.db.get('_design/' + this.ddocid)
            .then(function(ddoc) {
                if (ddoc.META && ddoc.META.version) {
                    self.ddoc = ddoc;
                    if (version && version !== ddoc.META.version) {
                        self.ddoc = {version: ddoc.META.version, rev: ddoc._rev};
                    }
                } else { self.ddoc = false; }
                return self.ddoc;
            },
            function() {
                self.ddoc = false;
                return false;
            });
        },
        extractLinks: function(content) {
            var links = [];
            this.$('a', marked(content)).each(function(i, e) {
                if ((e.protocol === location.protocol) && (e.hostname === location.hostname) && (e.port === location.port)) {
                    links.push(e.pathname.slice(1));
                }
            });
            return links;
        },
        pouchdbPlugins: function(type, plugins) {
            // type: only (intersection) OR remove (difference)
            // plugins: array of plugin names (by string)
            var actions = {'intersection': 'intersection', 'difference': 'difference', 'only': 'intersection', 'remove': 'difference'},
                allPlugins = ['Update', 'Validation', 'Show', 'List', 'Rewrite', 'Replicator'],
                self = this;

            if ('string' === typeof plugins) { plugins = [plugins]; }
            if (!plugins || !actions[type] || !this._.isArray(plugins)) { return; }
            this._.chain(allPlugins)[actions[type]](plugins)
            .filter(function(p) { return 'object' === typeof self.window[p]; })
            .each(function(p) { PouchDB.plugin(self.window[p]); });
        },
        setupAllDBs: function() {
            var self = this;

            PouchDB.allDbs().then(function(a) {
                var td1, td2, td3;
                if (a.length) { return; } // bail out, our job is already done
                // toastr...
                self.$('.page div img', self.$app).after('<p>La première initialisation peut prendre un certain temps. Merci de patienter.</p>');

                // first time, let's initialize our alldbs db properly
                if (self.window.indexedDB && self.window.indexedDB.webkitGetDatabaseNames) {
                    self.window.indexedDB.webkitGetDatabaseNames().onsuccess = function(sender) {
                        self._.chain(sender.target.result)
                        .reject(function(v) { return ('_pouch_pouch__all_dbs__' === v) || ('_pouch_' + self.dbname === v) || (-1 !== v.indexOf('-mrview-'));})
                        .map(function(v) { return v.slice(7); }) // 7 === '_pouch_'.length
                        .each(function(idb) {
                            var d = new PouchDB(idb);
                            d.info().then(function(i1) {
                                /* jshint camelcase: false */
                                if (!i1.update_seq) { d.destroy(); }
                            });
                        });
                    };
                } else {
                    // otherwise, we'll enumerate dbs based on known names
                    td1 = new PouchDB('tvd');
                    td1.info().then(function(i1) {
                        /* jshint camelcase: false */
                        if (!i1.update_seq) { td1.destroy(); }
                    });

                    td2 = new PouchDB('tvd-0.1.2');
                    td2.info().then(function(i1) {
                        /* jshint camelcase: false */
                        if (!i1.update_seq) { td2.destroy(); }
                    });

                    td3 = new PouchDB('tvd-0.1.9');
                    td3.info().then(function(i1) {
                        /* jshint camelcase: false */
                        if (!i1.update_seq) { td3.destroy(); }
                    });
                }
            });
        },
        init: function(mycb) {
            var self = this, options = this.config;
            options.ddocversion = options.ddocversion||'0.0.0';
            options.dbname = this.$.cookie('dbname')||options.dbname||'tvd-0.0.0';
            this.$app = this.$('#app');
            this.pouchdbPlugins(options.dbplugins.type, options.dbplugins.plugins);
            this.db = new PouchDB(options.dbname, {
                /* jshint camelcase: false */
                auto_compaction: true
            });
            this.dbname = options.dbname;

            this.setupAllDBs();
           
            this.checkDDoc(options.ddocversion).then(function(ddoc) {
                if (ddoc) {
                    if (ddoc.version && ddoc.rev) {
                        //toastr.warning('got design doc version ' + ddoc.version + ' but expecting version ' + options.ddocversion);                        
                        self.$.getScript('scripts/db/init.js').done(function() {
                            initDDoc(self, mycb, options.ddocversion, ddoc);
                        });
                    } else {
                        //toastr.success('got design doc!');
                        mycb();
                    }
                } else {
                    //toastr.warning('no design doc found');
                    self.$.getScript('scripts/db/init.js').done(function() {
                        initDDoc(self, mycb, options.ddocversion);
                    }).fail(function(a, b, c, d, e) {
                        console.log('FAIL', a, b, c, d, e);
                    });
                }
            });
        }
    };

/*
    PouchDB.on('created', function(a, b, c) {
        console.log('DB CREATED', a, b, c);
    });
*/
}(window));
