# Installation à partir de gitorious

On suppose que vous avez bower et npm.

```
git clone https://gitorious.org/medite/tvd.git
cd tvd
npm install && bower install # télécharger les dépendances
cd test
bower install
cd -
gulp serve # ctrl-c pour terminer
www-browser http://localhost:9000 # démarrer votre fureteur
gulp # va construire l'application dans dist/
cd dist
php -S localhost:9000 # utiliser PHP pour démarrer un serveur local
```
